import HeaderDark from '../components/Header/HeaderDark';

function Home() {
  return (
    <>
      <HeaderDark />
      <p>Hello World</p>
    </>
  );
}

export default Home;
