import mysql from 'mysql2/promise';
import connection from '../../utils/db/connection';
import bcrypt from 'bcryptjs';

export default async function login(req, res) {
  if (req.method === 'POST') {
    try {
      const dbConnection = await mysql.createConnection(connection);
      const json = JSON.parse(req.body);
      const { email, password } = json;
      const query = `SELECT * FROM users WHERE email = "${email}"`;
      console.log(query);
      const value = [];
      const [data] = await dbConnection.execute(query, value);
      dbConnection.end();

      if (data.length === 0) {
        res.status(200).json({ message: 'User Not Found' });
      } else {
        const dataUser = data[0];
        if (bcrypt.compareSync(password, dataUser.password)) {
          res.status(200).json({ message: 'User Success Login' });
        } else {
          res.status(200).json({ message: 'Wrong Password' });
        }
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
}
