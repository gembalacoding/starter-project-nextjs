import Link from 'next/link';
import Head from 'next/head';
import { useRef } from 'react';

export default function Login() {
  const emailLogin = useRef();
  const passwordLogin = useRef();
  const btnLogin = async () => {
    const data = {
      email: emailLogin.current.value,
      password: passwordLogin.current.value,
    };

    const jsonData = JSON.stringify(data);
    const options = {
      method: 'POST',
      Headers: {
        'Content-Type': 'application/json',
      },
      body: jsonData,
    };

    const response = await fetch('http://localhost:3000/api/login', options);
    const result = await response.json();
    console.log(result);
  };
  return (
    <>
      <Head>
        <title>Selamat Datang! | Halaman Login</title>
      </Head>
      <section className="height-100vh d-flex align-items-center page-section-ptb login login-gradient">
        <div className="container">
          <div className="row g-0 justify-content-center position-relative">
            <div
              className="col-lg-4 col-md-6 login-fancy-bg bg-overlay-black-30"
              style={{ background: 'url(/images/login/02.jpg)' }}
            >
              <div className="login-fancy pos-r">
                <h2 className="text-white mb-20">Hello world!</h2>
                <p className="mb-20 text-white">
                  Create tailor-cut websites with the exclusive multi-purpose
                  responsive template along with powerful features.
                </p>
                <ul className="list-unstyled list-inline-item pos-bot pb-30">
                  <li className="list-inline-item">
                    <Link href="/">
                      <a className="text-white"> Terms of Use</a>
                    </Link>
                  </li>
                  <li className="list-inline-item">
                    <a className="text-white"> Privacy Policy</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 white-bg">
              <div className="login-fancy pb-40 clearfix">
                <h3 className="mb-30">login</h3>
                <div className="section-field mb-20">
                  <p className="mb-10" htmlFor="name">
                    User name*
                  </p>
                  <input
                    ref={emailLogin}
                    id="name"
                    className="web form-control"
                    type="text"
                    placeholder="User name"
                    name="email"
                  />
                </div>
                <div className="section-field mb-20">
                  <p className="mb-10" htmlFor="Password">
                    Password*
                  </p>
                  <input
                    ref={passwordLogin}
                    id="Password"
                    className="Password form-control"
                    type="password"
                    placeholder="Password"
                    name="Password"
                  />
                </div>
                <div className="section-field">
                  <div className="remember-checkbox mb-30">
                    <a href="password-recovery.html" className="float-end">
                      Forgot Password?
                    </a>
                  </div>
                </div>
                <a className="button" onClick={() => btnLogin()}>
                  <span>Log in</span>
                  <i className="fa fa-check" />
                </a>
                <p className="mt-20 mb-0">
                  Dont have an account?
                  <Link href="registration">
                    <a> Create one here</a>
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
